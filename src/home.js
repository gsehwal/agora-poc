import React, {Component} from 'react';
import AgoraRTC from 'agora-rtc-sdk';

var rtc = {
	client: null,
	joined: false,
	published: false,
	localStream: null,
	remoteStreams: [],
	params: {
		uid: null
	}	
};

// Options for joining a channel
var option = {
	appID: "8073760c89954b7c85f9060cde2ca388",
	channel: "student",
	uid: null,
	token: "0068073760c89954b7c85f9060cde2ca388IAACrQ07PSyg9xcn81dHmgTGUpHw2wvWsZUiwq2KbnRzejOvI7cAAAAAEADikULNY4cNXwEAAQBkhw1f"
};

function addView(id, show) {
	let video = document.getElementById('video');
	let videoPanel = document.createElement('div');
	videoPanel.setAttribute('id', "remote_video_panel_" + id);
	videoPanel.setAttribute('class', "video-view");
	
	
	let remoteVideo = document.createElement('div');
	remoteVideo.setAttribute('id', "remote_video_" + id);
	remoteVideo.setAttribute('class', "video-placeholder");

	let videoInfo = document.createElement('div');
	videoInfo.setAttribute('id', "remote_video_info_" + id);
	videoInfo.setAttribute('class', "video-profile " + (show ? "" :  "hide"));
	

	videoPanel.appendChild(remoteVideo);
	videoPanel.appendChild(videoInfo);
	video.appendChild(videoPanel);
  }


class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	createClient() {
		rtc.client = AgoraRTC.createClient({mode: "rtc", codec: "h264"});
		this.subscribeStream();
		rtc.client.init(option.appID, function () {
			console.log("init success");
			rtc.client.join(option.token, option.channel, option.uid, function (uid) {
				console.log("join channel: " + option.channel + " success, uid: " + uid);
				rtc.params.uid = uid;
				rtc.localStream = AgoraRTC.createStream({
					streamID: rtc.params.uid,
					audio: true,
					video: true,
					screen: false,
				});
			}, function(err) {
				console.error("client join failed", err);
			});
			
		}, (err) => {
			console.error(err);
		});
	}

	publishStream() {
		rtc.localStream.init(function () {
			console.log("init local stream success");
			// play stream with html element id "local_stream"
			// rtc.localStream.play("local_stream");
			console.log(rtc)
			rtc.client.publish(rtc.localStream, function (err) {
				console.log("publish failed");
				console.error(err);
			  });
		}, function (err) {
			console.error("init local stream failed ", err);
		});
	}

	subscribeStream() {
		rtc.client.on("stream-added", function (evt) {  
			var remoteStream = evt.stream;
			var id = remoteStream.getId();
			if (id !== rtc.params.uid) {
				rtc.client.subscribe(remoteStream, function (err) {
					console.log("stream subscribe failed", err);
				});
			}
			console.log("stream-added remote-uid: ", id);
		});

		rtc.client.on("stream-subscribed", function (evt) {
			var remoteStream = evt.stream;
			var id = remoteStream.getId();
			// Add a view for the remote stream.
			addView(id);
			// Play the remote stream.
			console.log('remote stream', remoteStream);
			remoteStream.play("remote_video_" + id);
			console.log("stream-subscribed remote-uid: ", id);
		});
	}

	

	render() {
		return (
			<div>
				<button onClick={this.createClient.bind(this)}>Create client</button>
				<button onClick={this.publishStream.bind(this)}>Publish</button>
				<button onClick={this.subscribeStream.bind(this)}>Subscribe</button>
				<div id='video'></div>
			</div>
		)
	}
}

export default Home;